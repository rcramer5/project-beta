import React from "react";
import YoutubeEmbed from "./YoutubeEmbed";

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
      <div>
        
        <YoutubeEmbed embedId="evHTbyLKiWE" />
      </div>
    </div>
  );
}

export default MainPage;
