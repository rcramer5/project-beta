import React from "react";

class ModelList extends React.Component{
    constructor(props){
        super(props)
        this.state = {models: []}
    }

    async componentDidMount() {
        const modelUrl = "http://localhost:8100/api/models/";
        const response = await fetch(modelUrl);
        const data = await response.json()
        this.setState({models: data.models})
    }

    render() {
        return (
            <div>
                <br>
                </br>
                <h1>Vehicle Models</h1>
                <br></br>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Model Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map(model => (
                            <tr key={model.id}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td>
                                    <img className="photo" src={model.picture_url} alt=""/>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ModelList;