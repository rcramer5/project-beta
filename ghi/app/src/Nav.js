import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-dark navbar-custom">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon" ></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <div className="container">
            <div className='row'>
              <div className='col-lg-4'>
                <h3>Inventory</h3>
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/inventory">Automobile Inventory</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/inventory/automobiles/new">Add an Automobile to our Inventory</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/inventory/manufacturers">Manufacturers List</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/inventory/manufacturers/new">Add a Manufacturer</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/inventory/models">Vehicle Models List</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/inventory/models/new">Add a Vehicle Model</NavLink>
                  </li>
                </ul>
              </div>
              <div className='col-lg-4'>
                <h3>Sales</h3>
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/sales">Sales History</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/sales/new-sale">Record a Sale</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/sales/salesperson-history">Sales Person History</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/sales/new-salesperson">Register a New Sales Person</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/sales/new-customer">Register a New Customer</NavLink>
                  </li>
                </ul>
              </div>
              <div className='col-lg-4'>
                <h3>Services</h3>
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/service">Upcoming Services</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/service/new">Schedule a Service</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/service/history">Service History</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/technicians">Our Technicians</NavLink>
                  </li>
                  <li className='nav-item'>
                    <NavLink className="nav-link" to="/technicians/new">Register a New Technician</NavLink>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
