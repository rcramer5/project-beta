import React from "react";

class SaleRecordForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            customers: [],
            sales_people: [],
            automobiles: [],
            sale_price: "",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const value = event.target.value;
        const key = event.target.name;
        const changeDict = {}
        changeDict[key] = value;
        this.setState(changeDict);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.customers
        delete data.sales_people
        delete data.automobiles
        
        const saleUrl = "http://localhost:8090/api/sale_records/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const saleResponse = await fetch(saleUrl, fetchConfig);
        if (saleResponse.ok){
            const cleared = {
                customer: "",
                sales_person: "",
                automobile: "",
                sale_price: "",
            };
            this.setState(cleared);
        }
    }
    async componentDidMount(){
        const customerUrl = "http://localhost:8090/api/customers/";

        const customerResponse = await fetch(customerUrl)

        if (customerResponse.ok){
            const customerData = await customerResponse.json()
            this.setState({customers: customerData.customers})
        }
        const salesPersonUrl = "http://localhost:8090/api/sales_person/";

        const salesPersonResponse = await fetch(salesPersonUrl)

        if (salesPersonResponse.ok){
            const salesPersonData = await salesPersonResponse.json()
            this.setState({sales_people: salesPersonData.sales_people})
        }
        const automobileUrl = "http://localhost:8090/api/available/autos/";

        const automobileResponse = await fetch(automobileUrl)

        if (automobileResponse.ok){
            const automobileData = await automobileResponse.json()
            this.setState({automobiles: automobileData.available_autos})
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Record a Sale </h1>
                        <form onSubmit={this.handleSubmit} id="create-sale-record-form">
                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.customer} required id="customer" name="customer" className="form-control">
                                    <option value="">Choose a Customer</option>
                                    {this.state.customers.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>
                                                {customer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.sales_person} required id="sales_person" name="sales_person" className="form-control">
                                    <option value="">Choose a Sales Person</option>
                                    {this.state.sales_people.map(sales_person => {
                                        return (
                                            <option key={sales_person.id} value={sales_person.id}>
                                                {sales_person.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.automobile} required id="automobile" name="automobile" className="form-control">
                                    <option value="">Choose an Automobile VIN from our Inventory</option>
                                    {this.state.automobiles.map(automobile => {
                                        return (
                                            <option key={automobile.import_href} value={automobile.import_href}>
                                                {automobile.vin}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.sale_price} placeholder="Sale Price" required type="text" id="sale_price" name="sale_price" className="form-control"/>
                                <label htmlFor="sale_price">Sale Price ($)</label>
                            </div>
                            <button className="btn btn-success">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default SaleRecordForm;