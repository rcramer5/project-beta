import React, { Component } from "react";

class TechnicianForm extends React.Component
{
    constructor(props){
        super(props)
        this.state = {
            name:"",
            employee_number: "",
            technicians: [],
            employeeNumbers: [],
            unique: "unique",
            numberLength: "not valid"
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChangeName = this.handleChangeName.bind(this)
        this.handleChangeEmployeeNumber = this.handleChangeEmployeeNumber.bind(this)
    }

    async handleSubmit(event)
    {
        event.preventDefault()
        const data = {...this.state}
        delete data.technicians
        delete data.employeeNumbers
        delete data.unique
        delete data.numberLength
        const technicianUrl = 'http://localhost:8080/api/technicians/'
        const fetchConfig = 
        {
            method:"post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(technicianUrl, fetchConfig)
        console.log(response)
        if (response.ok)
        {
            const newTechnician = await response.json()
            this.setState({
                name:'',
                employee_number:'',
                unique:"unique",
                employeeNumbers: [],
            })
        }
    }
    handleChangeName(event)
    {
        const value = event.target.value
        this.setState({name: value})
    }
    handleChangeEmployeeNumber(event)
    {
        console.log("handlgechangeemployeenumber ran")
        const value = event.target.value
        this.setState({employee_number: value})
        let data = {...this.state}
        console.log("employeenumber=" + employee_number.value)
        if (data.employeeNumbers.includes(employee_number.value) === true)
        {
            
            this.setState({unique: "not unique"})
            console.log(this.state.unique)
        }
        else
        {
            this.setState({unique: "unique"})
            console.log(value)
            console.log(data.employeeNumbers)
        }
        if (employee_number.value.length === 5)
        {
            this.setState({numberLength:  "valid"})
        }
        else
        {
            this.setState({numberLength:  "not valid"})
        }
    }
    
    async componentDidMount()
    {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url)
        if (response.ok)
        {
            const data = await response.json()
            console.log("component did mount")
            console.log(data)
            this.setState({technicians: data.technicians})
            const employeeNumbers = data.technicians.map(({employee_number})=>employee_number)
            console.log(employeeNumbers)
            this.setState({employeeNumbers: employeeNumbers})
        }
    }
    
    // numberValid({unique}, {employee_number}){
    //     {(function(){
            
    //         if ({unique}=== true && {employee_number}.length === 5)
    //         {
    //             return(<p className="text-success">This is a valid employee number</p>)
    //         }
    //          else if ({unique} === true && {employee_number}.length !== 5)
    //         {
    //             return(
    //             <div>
    //                     <p className="text-success"> Number is unique</p>
    //                 <p className="text-danger"> number must be 5 digits</p>
    //             </div>
    //             )
    //         }
    //         else if ({unique} === false && {employee_number}.length === 5)
    //         {
    //             return(
    //                 <div>
    //                     <p className="text-danger"> Number is not unique</p>
    //                     <p className="text-success"> Number correct length</p>
    //                 </div>
    //                 )
    //         }else
    //                         {
    //             return(<p className="text">Employee number should be 5 digits and must be unique</p>)
    //         }
    //     })()}
    // }


    render()
    {
        let unique = this.state.unique
        
        let numberLength = this.state.numberLength
        let uniqueValue
        let lengthValid
        if (this.state.unique == "unique")
        {
            console.log("in the if")
            uniqueValue = <div className="text-success"><p className= "font-weight-bold">{this.state.unique}</p></div>
        }
        else if (this.state.unique == "not unique" )
        {
            console.log("in the else")
            uniqueValue = <div className="text-danger"><p className= "font-weight-bold">{this.state.unique}</p></div>
        }
        if (this.state.numberLength == "valid")
        {
            lengthValid = <div className="text-success"><p className= "font-weight-bold">{this.state.numberLength}</p></div>
        }
        else
        {
            lengthValid = <div className="text-danger"><p className= "font-weight-bold">{this.state.numberLength}</p></div>
        }
        
        return(
            <>
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Register a New Technician</h1>
                  <form onSubmit={this.handleSubmit} id="create-service-form">
                    <div className="form-floating mb-3">
                      <input value={this.state.name} onChange={this.handleChangeName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                      <label htmlFor="name">Employee Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input value={this.state.employee_number} onChange={this.handleChangeEmployeeNumber} placeholder="EmployeeNumber" required type="text" name="employee_number" id="employee_number" className="form-control" />
                      <label htmlFor="employee_number">Employee Number</label>
                        {/* {this.numberValid({unique}, {employee_number})} */}
                       <div className= "float-left"><p>The current employee number is </p></div>
                        {uniqueValue}
                       <div className= "float-left"><p>The current employee number length is </p></div> 
                        {lengthValid}
                    </div>
                    <button onClick = {this.handleSubmit} className="btn btn-success">Create Employee</button>
                  </form>
                </div>
              </div>
            </div>
            </>
          );

    }
}
export default TechnicianForm;