import React, { Component } from "react";

class TechnicianList extends Component
{
    constructor(props)
    {
        super(props)
        this.state = 
        {
            technicians: []
        }
    }
    async componentDidMount()
    {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url)

        if (response.ok)
        {
            const data = await response.json()
            this.setState({technicians: data.technicians})
        }
    }
    render()
    {
        return(
        <>
        <h1>Active Technicians</h1>
        <table className="table table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee ID</th>

                </tr>
            </thead>
            <tbody>
                {this.state.technicians.map(technician =>
                    {
                        return(
                        <tr key={technician.id}>
                            <td>{technician.name}</td>
                            <td>{technician.employee_number}</td>
                        </tr>
                    )})}
            </tbody>
        </table>
        </>
    )}
}
export default TechnicianList;