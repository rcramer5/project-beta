from django.urls import path, include
from .api_views import api_list_available_automobiles, api_list_sale_records, api_create_customer, api_create_sales_person

urlpatterns = [
    path("sale_records/", api_list_sale_records, name="api_list_sale_records"),
    path("customers/", api_create_customer, name="api_create_customer"),
    path("sales_person/", api_create_sales_person, name="api_create_sales_person"),
    path("available/autos/", api_list_available_automobiles, name="api_list_available_automobiles"),
]
